#!/bin/bash
set -xeuo pipefail

umask "${OCP_BACKUP_UMASK}"

case "${OCP_BACKUP_EXPIRE_TYPE}" in
    days|count|never) ;;
    *) echo "backup.expiretype needs to be one of: days,count,never"; exit 1 ;;
esac

if [ "${OCP_BACKUP_EXPIRE_TYPE}" = "days" ]; then
  case "${OCP_BACKUP_KEEP_DAYS}" in
    ''|*[!0-9]*) echo "backup.expiredays needs to be a valid number"; exit 1 ;;
    *) ;;
  esac
elif [ "${OCP_BACKUP_EXPIRE_TYPE}" = "count" ]; then
  case "${OCP_BACKUP_KEEP_COUNT}" in
    ''|*[!0-9]*) echo "backup.expirecount needs to be a valid number"; exit 1 ;;
    *) ;;
  esac
fi

BACKUP_FOLDER="$( date "${OCP_BACKUP_DIRNAME}")" || { echo "Invalid backup.dirname" && exit 1; }
BACKUP_PATH="$( realpath -m "${OCP_BACKUP_SUBDIR}/${BACKUP_FOLDER}" )"
BACKUP_PATH_POD="$( realpath -m "/backup/${BACKUP_PATH}" )"
BACKUP_ROOTPATH="$( realpath -m "/backup/${OCP_BACKUP_SUBDIR}" )"

mkdir -p "/host/var/tmp/etcd-backup"
mkdir -p "${BACKUP_PATH_POD}"

chroot /host /usr/local/bin/cluster-backup.sh /var/tmp/etcd-backup

mv /host/var/tmp/etcd-backup/* "${BACKUP_PATH_POD}"
rm -rv /host/var/tmp/etcd-backup

if [ "${OCP_BACKUP_EXPIRE_TYPE}" = "days" ]; then
  find "${BACKUP_ROOTPATH}" -mindepth 1 -maxdepth 1  -type d -mtime "+${OCP_BACKUP_KEEP_DAYS}" -exec rm -rv {} +
elif [ "${OCP_BACKUP_EXPIRE_TYPE}" = "count" ]; then
  ls -1tp "${BACKUP_ROOTPATH}" | awk "NR>${OCP_BACKUP_KEEP_COUNT}" | xargs -I{} rm -rv "${BACKUP_ROOTPATH}/{}"
fi
